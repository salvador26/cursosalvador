package com.disney.projects.interfaces.impl;

import java.util.List;
import java.util.Scanner;

import com.disney.projects.abstracts.Chocolate;
import com.disney.projects.abstracts.Products;
import com.disney.projects.abstracts.Soda;
import com.disney.projects.interfaces.ProductsService;

public class ProductsServiceImpl implements ProductsService {
	
	private Scanner in;
	private double totalSodas = 0.0;
	private double totalChocolates = 0.0;
	private double subtotal = 0.0;
	private double total = 0.0;
	private double discount = 0.0;
	private int quantity;

	public List<Products> createList(List<Products> products, Soda soda, Chocolate chocolate) {

		products.add(soda);
		products.add(chocolate);
		

		return products;

	}

	public double addProducts(List<Products> products, List<Products> productsAdd) {

		in = new Scanner(System.in);
		
		System.out.println("the products" + products);
		System.out.println("¿What do you want to do 1-Soda 2-Chocolate?");
		int answer = in.nextInt();

		

		switch (answer) {

		case 1:
			System.out.println("¿how many you want to buy?");
			quantity = in.nextInt();
			
			System.out.println("added soda");
			Soda soda = new Soda("coca cola", 13.50, quantity);
			productsAdd.add(soda);
			soda.calculateStock(products, soda.getName(), quantity);
			totalSodas = soda.calculateWholesale(productsAdd);
			System.out.println("products added"+productsAdd);

			break;
		case 2:

			System.out.println("added chocolate");
			System.out.println("¿how many you want to buy?");
			quantity = in.nextInt();
			Chocolate chocolate = new Chocolate("carlos v", 12,quantity);
			productsAdd.add(chocolate);

			chocolate.calculateStock(products, chocolate.getName(), quantity);
			totalChocolates = chocolate.calculateWholesale(productsAdd);

			break;

		default:

			System.out.println("option invalid");

			break;
		}

		subtotal = totalSodas + totalChocolates;

		System.out.println("subtotal***" + subtotal);
		return subtotal;
	}

	public double calculateDiscount(double subtotal) {

		if (subtotal >= 1000 && subtotal <= 3000) {
			System.out.println("30 %");
			discount = subtotal * 30 / 100;
			total = subtotal - discount;
		} else if (subtotal > 3000) {
			System.out.println("50 %");
			discount = subtotal * 50 / 100;
			total = subtotal - discount;

		} else {
			System.out.println("there is no discount %");
			total = subtotal;

		}
		
		return total;

		

	}

	public void printResults(List<Products> products, List<Products> productsAdd, double total) {
		System.out.println("products added" + productsAdd);
		System.out.println("products in stock" + products);
		System.out.println("total = $" + total);
	}

}
