package com.disney.projects.interfaces;

import java.util.List;

import com.disney.projects.abstracts.Chocolate;
import com.disney.projects.abstracts.Products;
import com.disney.projects.abstracts.Soda;

public interface ProductsService {

	List<Products> createList(List<Products> products, Soda soda, Chocolate chocolate);

	double addProducts(List<Products> products, List<Products> productsAdd);

	double calculateDiscount(double subtotal);

	void printResults(List<Products> products, List<Products> productsAdd, double total);

}
