package com.disney.projects.beans;

import com.disney.projects.interfaces.Figurables;

public class Triangle extends Figure implements Figurables {


    private static final int TRINAGLE_SIDES = 3;
    private float base;
    private float tall;
    
    String defaultExample = "examplePackageAccess";

    public Triangle(float base, float tall) {
        this.base = base;
        this.tall = tall;
    }

    public void calculateArea() {
        this.setArea(base* tall);
    }

    public void calculatePerimeter() {
        this.setPerimeter(base * TRINAGLE_SIDES);
    }

    public float getBase() {
        return base;
    }

    public void setBase(float base) {
        this.base = base;
    }

    public float getTall() {
        return tall;
    }

    public void setTall(float tall) {
        this.tall = tall;
    }


}
