package com.disney.projects.beans;

public class SecondSon extends Father {

	public SecondSon(String name, int age) {
		super(name, age);
	}

	@Override
	public void playSoccer() {

		System.out.println("Father**************");
		super.playSoccer();

		System.out.println("**************");

		System.out.println("play soccer excelent as messi");
	}

	@Override
	public String toString() {
		return "SecondSon[name=" + getName() + " age=" + getAge() + "]";
	}

}
