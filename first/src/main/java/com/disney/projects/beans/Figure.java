package com.disney.projects.beans;

public  class Figure {

    private float area;
    private float perimeter;
    
    public float getArea() {
        return area;
    }
    
    
    public  void  setArea(float area) {
        this.area = area;
    }
    
    
    public float getPerimeter() {
        return perimeter;
    }
    public void setPerimeter(float perimeter) {
        this.perimeter = perimeter;
    }

    

}
