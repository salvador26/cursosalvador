package com.disney.projects.beans;

public class FirstSon extends Father {

	public FirstSon(String name, int age) {
		super(name, age);
	}

	@Override
	public void playSoccer() {

		System.out.println("Father**************");
		super.playSoccer();

		System.out.println("**************");

		System.out.println("play soccer bad as pikolin");
	}

	@Override
	public String toString() {
		return "FirstSon[name=" + getName() + " age=" + getAge() + "]";
	}

}
