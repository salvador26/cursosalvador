package com.disney.projects.beans;

import com.disney.projects.interfaces.Figurables;

public class Square extends Figure implements Figurables {

    private static final int SQUARE_SIDES = 4;

    private float side;


    public Square(int side) {
        this.side = side;
    }

    public void calculateArea() {
        this.setArea(side * side);
    }

    public void calculatePerimeter() {
        this.setPerimeter(side * SQUARE_SIDES);
    }

    public float getSide() {
        return side;
    }

    public void setSide(float side) {
        this.side = side;
    }

}
