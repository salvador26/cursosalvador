package com.disney.projects.abstracts;

public abstract class PetShop {

	private String name;
	private double weight;

	public PetShop() {

	}

	public PetShop(String name, double weight) {
		this.name = name;
		this.weight = weight;
	}

	public void purchasePayment() {

		System.out.println("hi my name is " + getName());
		System.out.println("my weight is " + getWeight() + "kg");

		System.out.println("this is your purchasePayment");

		calculatePurchasePayment();

	}

	public abstract void calculatePurchasePayment();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	@Override
	public String toString() {
		return "Petshop [name=" + name + ", age=" + weight + "]";
	}

}
