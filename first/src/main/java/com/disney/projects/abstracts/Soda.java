package com.disney.projects.abstracts;

import java.util.List;

public class Soda extends Products {

	public Soda(String name, double price, int stock) {
		super(name, price, stock);
	}

	@Override
	public void calculateStock(List<Products> products, String name, int cantidad) {

		for (Products products2 : products) {
			if (products2.getName() == name) {

				products2.setStock(products2.getStock() - cantidad);

			}

		}
	}

	@Override
	public double calculateWholesale(List<Products> productsAdd) {

		int quantity = 0;
		double price = 0.0;
		double totalCocas;
		for (Products products : productsAdd) {
			if (products.getName().equals("coca cola")) {

				quantity += products.getStock();
			}

			price = products.getPrice();
		}
		if (quantity > 100) {

			System.out.println("wholesale price change in coca cola ");

			for (Products products2 : productsAdd) {
				products2.setPrice(10);

				price = products2.getPrice();

			}
			totalCocas = quantity * price;

			return totalCocas;

		} else {

			totalCocas = quantity * price;

			return totalCocas;
		}
	}

}
