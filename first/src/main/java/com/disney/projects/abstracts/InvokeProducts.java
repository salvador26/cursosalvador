package com.disney.projects.abstracts;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.disney.projects.interfaces.impl.ProductsServiceImpl;

public class InvokeProducts {
	private static Scanner in;

	private static ProductsServiceImpl productsService = new ProductsServiceImpl();

	public static void main(String[] args) {

		in = new Scanner(System.in);

		Soda soda = new Soda("coca cola", 13.50, 1000);
		Chocolate chocolate = new Chocolate("carlos v", 12, 500);
		List<Products> products = new ArrayList<Products>();
		List<Products> productsAdd = new ArrayList<Products>();

		products = productsService.createList(products, soda, chocolate);
		
		int optionMenu;
		do {

			double subtotal = productsService.addProducts(products, productsAdd);
			double total = productsService.calculateDiscount(subtotal);
			productsService.printResults(products, productsAdd, total);
			System.out.println("¿You want to perform another operation 1-Yes 2-No ?");
			optionMenu = in.nextInt();

		} while (optionMenu == 1);

	}

}
