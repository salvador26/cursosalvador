package com.disney.projects.abstracts;

import java.util.List;

public abstract class Products {

	private String name;
	private double price;
	private int stock;

	public Products() {

	}

	public Products(String name, double price, int stock) {
		this.name = name;
		this.price = price;
		this.stock = stock;
	}

	public abstract void calculateStock(List<Products> products, String name, int cantidad);
	
	public abstract double calculateWholesale(List<Products> productsAdd);

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	@Override
	public String toString() {
		return "Products [name=" + name + ", price=" + price + ", stock=" + stock + "]";
	}

}
