package com.disney.projects.abstracts;

import java.util.List;

public class Chocolate extends Products {

	public Chocolate(String name, double price, int stock) {
		super(name, price, stock);
	}

	@Override
	public void calculateStock(List<Products> products, String name, int cantidad) {
		System.out.println("entro al metodo de abstract chocolate");

		for (Products products2 : products) {
			if (products2.getName() == name) {

				products2.setStock(products2.getStock() - cantidad);

			}

		}

	}

	@Override
	public double calculateWholesale(List<Products> productsAdd) {
		int quantity = 0;
		double price = 0.0;
		double totalChocolate;

		for (Products products : productsAdd) {
			if (products.getName().equals("carlos v")) {
				quantity += products.getStock();

			}
			price = products.getPrice();
		}

		if (quantity > 100) {

			System.out.println("wholesale price change in coca cola");

			for (Products products2 : productsAdd) {
				products2.setPrice(9.50);

				price = products2.getPrice();

			}
			totalChocolate = quantity * price;

			return totalChocolate;

		} else {

			totalChocolate = quantity * price;

			return totalChocolate;
		}
	}
}