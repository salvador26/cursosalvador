package com.disney.projects.abstracts;

import java.util.Scanner;

public class HierachyInvokerATM {

	public static final int MIL = 1000;
	public static final int QUINIENTOS = 500;
	public static final int DOSCIENTOS = 200;
	public static final int CIEN = 100;
	public static final int CINCUENTA = 50;
	static Scanner in = new Scanner(System.in);

	public static void main(String[] args) {

		int optionMenu;

		HierachyInvokerATM obj = new HierachyInvokerATM();

		do {

			System.out.println("*WELCOME TO BANCOMER*");
			System.out.println("* The cashier only gives bills of $1000, $500, $200, $100, $50 *");
			obj.calculateCashWithdrawal();

			System.out.println("¿You want to perform another operation 1-Yes 2-No ?");
			optionMenu = in.nextInt();

		} while (optionMenu == 1);

	}

	public void calculateCashWithdrawal() {

		int bill = 0;

		System.out.println("How much do you want to withdraw?");
		int number = in.nextInt();

		if (number >= CINCUENTA) {

			if (number >= MIL) {

				bill = number / MIL;
				number = number % MIL;

				System.out.println("are: " + bill + " bills of $" + MIL);
			}
			if (number >= QUINIENTOS) {

				bill = number / QUINIENTOS;
				number = number % QUINIENTOS;

				System.out.println("are: " + bill + " bills of $" + QUINIENTOS);

			}

			if (number >= DOSCIENTOS) {
				bill = number / DOSCIENTOS;
				number = number % DOSCIENTOS;

				System.out.println("are: " + bill + " bills of $" + DOSCIENTOS);
			}

			if (number >= CIEN) {

				bill = number / CIEN;
				number = number % CIEN;

				System.out.println("are: " + bill + " bills of $" + CIEN);
			}
			if (number >= CINCUENTA) {

				bill = number / CINCUENTA;
				number = number % CINCUENTA;

				System.out.println("are: " + bill + " bills of $" + CINCUENTA);
			}

		} else {

			System.out.println("You can not withdraw that amount");
		}

	}

}
