package com.disney.projects.abstracts;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

public class HierachyInvokerExerciseDogs {

	private static Scanner in;
	private static Map<Integer, PetShop> dogs;

	public static void main(String[] args) {

		in = new Scanner(System.in);
		dogs = new HashMap<Integer, PetShop>();

		Bulldog bulldog = new Bulldog("chop", 8.500);
		Pug pug = new Pug("corraje", 2.400);

		dogs.put(1, bulldog);
		dogs.put(2, pug);

		int optionMenu;
		String name;
		int age;
		Integer key;

		do {

			HierachyInvokerExerciseDogs animals = new HierachyInvokerExerciseDogs();

			System.out.println("### Welcome ###");

			animals.imprimeDogs();

			System.out.println("¿What do you want to do 1-Add a objetc 2-Delete a object");
			int respuesta = in.nextInt();

			switch (respuesta) {
			case 1:

				System.out.println("¿Which player do you want add?");

				System.out.println("Enter key:");
				key = in.nextInt();

				dogs.put(key, animals.createObj());

				break;

			case 2:

				System.out.println("¿Which player do you want delete?");

				System.out.println("Enter key:");
				key = in.nextInt();

				dogs.remove(key);

				animals.imprimeDogs();

				break;

			default:

				System.out.println("option invalid");

				break;
			}

			System.out.println("¿You want to perform another operation 1-Yes 2-No ?");
			optionMenu = in.nextInt();

		} while (optionMenu == 1);

	}

	public Bulldog createObj() {

		System.out.println("Enter name:");
		String name = in.next();

		System.out.println("Enter weigth:");
		double weigth = in.nextDouble();

		Bulldog obj = new Bulldog(name, weigth);

		return obj;
	}

	public void imprimeDogs() {
		for (Entry<Integer, PetShop> dog : dogs.entrySet()) {
			Integer clave = dog.getKey();
			PetShop valor = dog.getValue();
			System.out.println(clave + "  ->  " + valor.toString());
		}
	}

}
