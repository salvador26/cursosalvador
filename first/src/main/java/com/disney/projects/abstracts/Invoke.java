package com.disney.projects.abstracts;

import java.util.ArrayList;
import java.util.List;

public class Invoke {

	public static void main(String[] args) {

		Bulldog bulldog = new Bulldog("chop", 8.500);

		Pug pug = new Pug("corraje", 2.400);

		System.out.println("*today they bought several dogs*");

		List<PetShop> petshop = new ArrayList<PetShop>();

		petshop.add(bulldog);
		petshop.add(pug);

		for (PetShop petShop2 : petshop) {
			petShop2.purchasePayment();
			System.out.println(":::::::::::::::::::::::");
		}

	}

}
