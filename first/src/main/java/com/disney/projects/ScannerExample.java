package com.disney.projects;

import java.util.Scanner;

public class ScannerExample {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner scanner = new Scanner(System.in);

		System.out.println("What is your name?");

		String name = scanner.next();

		System.out.println("How old are you?");

		int years = scanner.nextInt();
		
		System.out.println("My name is:"+name);
		System.out.println("you have "+years+" years old");
		

	}

}
