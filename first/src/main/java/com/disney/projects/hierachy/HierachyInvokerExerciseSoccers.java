package com.disney.projects.hierachy;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.disney.projects.beans.Father;
import com.disney.projects.beans.FirstSon;
import com.disney.projects.beans.SecondSon;

public class HierachyInvokerExerciseSoccers {

	private static Scanner in;
	private static List<Father> players;

	public static void main(String[] args) {

		in = new Scanner(System.in);
		players = new ArrayList<Father>();

		Father messi = new Father("Messi", 31);
		FirstSon cristiano = new FirstSon("Cristiano Ronaldo", 34);
		SecondSon chicharito = new SecondSon("Chicharito", 30);

		players.add(messi);
		players.add(cristiano);
		players.add(chicharito);

		int optionMenu;
		String name;
		int age;

		do {

			HierachyInvokerExerciseSoccers soccers = new HierachyInvokerExerciseSoccers();
			System.out.println("### Welcome ###");

			System.out.println("The players are: " + players);

			System.out.println("¿What do you want to do 1-Add a objetc 2-Delete a object");
			int respuesta = in.nextInt();

			switch (respuesta) {
			case 1:

				System.out.println("¿Which player do you want add?");

				players.add(soccers.createObj());

				System.out.println(players);

				break;

			case 2:

				System.out.println("¿Which player do you want delete?");

				players.remove(soccers.createObj());

				System.out.println(players);

				break;

			default:

				System.out.println("option invalid");

				break;
			}

			System.out.println("¿You want to perform another operation 1-Yes 2-No ?");
			optionMenu = in.nextInt();

		} while (optionMenu == 1);

	}

	public Father createObj() {

		System.out.println("Enter name:");
		String name = in.next();

		System.out.println("Enter age:");
		int age = in.nextInt();

		Father obj = new Father(name, age);

		return obj;
	}

}
