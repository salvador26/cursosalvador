package com.disney.projects.hierachy;

import java.util.ArrayList;
import java.util.List;

import com.disney.projects.beans.Father;
import com.disney.projects.beans.FirstSon;
import com.disney.projects.beans.SecondSon;

public class HierachyInvokerSoccers {

	public static void main(String[] args) {

		Father father = new Father("Salvador", 23);

		Father fatherTwo = new Father("Salvador", 23);

		System.out.println(father.getAge());

		father.playSoccer();

		FirstSon firstSon = new FirstSon("andresito", 21);

		firstSon.playSoccer();

		SecondSon secondSon = new SecondSon("williancito", 27);

		secondSon.playSoccer();

		System.out.println("-------------------------");

		System.out.println(father);

		System.out.println(firstSon);

		System.out.println(secondSon);

		List<Father> parents = new ArrayList<Father>();

		parents.add(father);
		parents.add(firstSon);
		parents.add(secondSon);

		System.out.println(parents);

		if (father.equals(fatherTwo)) {
			System.out.println("Are equals");
		} else {

			System.out.println("Not Are equals");

		}

		parents.remove(fatherTwo);

		System.out.println(parents);

	}

}
