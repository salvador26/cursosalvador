<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>

<%-- <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> --%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>


<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
	integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
	crossorigin="anonymous">
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
	integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
	crossorigin="anonymous"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<title>My First</title>
</head>

<body>
	<h2>${title}</h2>
	<form action="figuras" method="POST">

		<div class="form-check form-check-inline">
			<input class="form-check-input" type="radio" name="radio" id="square"
				value="1"> <label class="form-check-label"
				for="inlineRadio1">Square</label>
		</div>
		<div class="form-check form-check-inline">
			<input class="form-check-input" type="radio" name="radio"
				id="triangle" value="2"> <label class="form-check-label"
				for="inlineRadio2" onclick="window.location.reload()">Triangle</label>
		</div>


		<div class="form-group col-md-4">
			<label for="txt-numberOne">Side:</label> <input id="side" type="text"
				name="side" class="form-control" />
		</div>
		<div class="form-group col-md-4">
			<label for="baseLabel">base:</label> <input id="base" type="text"
				name="base" class="form-control" />

		</div>
		<div class="form-group col-md-4">
			<label for="tallLabel">tall:</label> <input id="tall" type="text"
				name="tall" class="form-control" />

		</div>
		<div class="form-group col-md-4">
			<input type="submit" value="Send" class="btn btn-primary"> <input
				type="button" value="Ocultar" id="ocultar" class="btn btn-primary">
			<input type="button" value="Mostrar" id="mostrar"
				class="btn btn-primary">
		</div>


	</form>

	<hr />

	<h2 id="fullData">${fullData}</h2>

	<script type="text/javascript">
		$(document).ready(function() {

			$('#square').click(function() {
				$('#tall').hide("slow");
				$('#base').hide("slow");
				$('#side').show();

			});

			$('#triangle').click(function() {
				$('#side').hide("slow");
				$('#tall').show();
				$('#base').show();
			});

			$('#ocultar').click(function() {
				$('#fullData').hide("slow");
			});

			$('#mostrar').click(function() {
				$('#fullData').show();
			});

		});
	</script>

</body>
</html>