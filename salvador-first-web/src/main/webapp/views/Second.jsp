<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>

<%-- <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> --%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>


<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
	integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
	crossorigin="anonymous">
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
	integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
	crossorigin="anonymous"></script>
<title>My First</title>
</head>

<body>

	<form action="secondPage" method="POST">
		
		<div class="form-group col-md-4">
			<label for="txt-numberOne">Name:</label> <input id="txt-numberOne"
				type="text" name="name" class="form-control" />
		</div>
		<div class="form-group col-md-4">
			<label for="txt-numberTwo">Age:</label> <input id="txt-numberTwo"
				type="text" name="age" class="form-control" />

		</div>
		<div class="form-group col-md-4">
			<input type="submit" value="Send" class="btn btn-primary">
		</div>

	</form>

	<hr />

	<!-- 	<div class="col-md-4"> -->
	<!-- 		<table class="table"> -->

	<!-- 			<thead> -->
	<!-- 				<tr> -->

	<!-- 					<th scope="col">Name</th> -->
	<!-- 					<th scope="col">Age</th> -->
	<!-- 				</tr> -->
	<!-- 			</thead> -->
	<%-- 			<c:forEach items="${lista}" var="item"> --%>
	<!-- 				<tr> -->
	<%-- 					<td>${item.name}</td> --%>
	<%-- 					<td>${item.age}</td> --%>
	<!-- 				</tr> -->
	<%-- 			</c:forEach> --%>
	<!-- 		</table> -->
	<!-- 	</div> -->

</body>
</html>