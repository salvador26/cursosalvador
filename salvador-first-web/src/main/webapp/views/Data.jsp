<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<title>example</title>
</head>
<body>
	<h1 align="center">Multiply two numbers</h1>
	<form action="firstData" method="POST">
		<div class="form-group col-md-4">
			<label for="txt-numberOne">Number One:</label> <input
				id="txt-numberOne" type="text" name="numberOne" class="form-control" />
		</div>
		<div class="form-group col-md-4">
			<label for="txt-numberTwo">Number Two:</label> <input
				id="txt-numberTwo" type="text" name="numberTwo" class="form-control" />

		</div>
		<div class="form-group col-md-4">
			<input type="submit" value="Send" class="btn btn-primary">
		</div>

	</form>
	<h2>${fullData}</h2>

</body>
</html>