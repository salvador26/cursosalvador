package com.disney.firstWebSalvador.exceptions;

public class FolioRepeatException extends RuntimeException{

	private static final long serialVersionUID = 1L;

	
	public FolioRepeatException (){
		super();
	}
	
	public FolioRepeatException (String msg){
		super(msg);
	}
	
	
	public FolioRepeatException (Throwable throwable){
		super(throwable);
	}
	
	public FolioRepeatException (String msg, Throwable throwable){
		super(msg,throwable);
	}
	
}
