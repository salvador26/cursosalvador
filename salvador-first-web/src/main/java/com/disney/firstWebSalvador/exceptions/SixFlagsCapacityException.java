package com.disney.firstWebSalvador.exceptions;

public class SixFlagsCapacityException extends Exception{

	private static final long serialVersionUID = 1L;

	
	public SixFlagsCapacityException (){
		super();
	}
	
	public SixFlagsCapacityException (String msg){
		super(msg);
	}
	
	
	public SixFlagsCapacityException (Throwable throwable){
		super(throwable);
	}
	
	public SixFlagsCapacityException (String msg, Throwable throwable){
		super(msg,throwable);
	}
	
}
