package com.disney.firstWebSalvador.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class FirstData
 */
public class FirstData extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor.
	 */
	public FirstData() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setAttribute("name", "Salvador");
		request.getRequestDispatcher("/views/Data.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		double numberOne = Double.valueOf(request.getParameter("numberOne"));

		double numberTwo = Double.valueOf(request.getParameter("numberTwo"));

		double result = numberOne * numberTwo;

		String fullData = "The result multiplication is = " + result;

		request.setAttribute("fullData", fullData);

		request.getRequestDispatcher("/views/Data.jsp").forward(request, response);

	}

}
