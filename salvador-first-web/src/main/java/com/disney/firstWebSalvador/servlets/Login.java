package com.disney.firstWebSalvador.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.disney.projects.beans.Square;
import com.disney.projects.beans.Triangle;

/**
 * Servlet implementation class FigurasData
 */
@WebServlet("/login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor.
	 */
	public Login() {
		// TODO Auto-generated constructor stub
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setAttribute("title", "Login");
		request.getRequestDispatcher("/views/Login.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String user = String.valueOf(request.getParameter("user"));
		String password = String.valueOf(request.getParameter("password"));

		if (user.equals("chava") && password.equals("123")) {
			request.setAttribute("welcome", "Welcome to the dbz:");
			request.getRequestDispatcher("/views/Bienvenido.jsp").forward(request, response);
		} else {
			request.setAttribute("title", "Login");
			request.setAttribute("fullData", "Login Incorrect verify password or user!!!");
			request.getRequestDispatcher("/views/Login.jsp").forward(request, response);
		}

	}

}
