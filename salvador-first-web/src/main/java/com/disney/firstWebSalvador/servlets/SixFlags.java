package com.disney.firstWebSalvador.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.disney.firstWebSalvador.exceptions.FolioRepeatException;
import com.disney.firstWebSalvador.exceptions.SixFlagsCapacityException;
import com.disney.firstWebSalvador.utils.ServletUtils;

@WebServlet("/sixflags")
public class SixFlags extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	ArrayList<String> listFolios = new ArrayList<String>();

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		
			request.getRequestDispatcher("/views/sixflags.jsp").forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		

		String folio = (String) request.getParameter("folio");
		try {

			ServletUtils.validateFolioRepeat(listFolios, folio);
			ServletUtils.validateCapacityUsers(listFolios);

			listFolios.add(folio);
		} catch (FolioRepeatException f) {
			request.setAttribute("error", "folio repeated");
			f.printStackTrace();
		}catch (SixFlagsCapacityException e) {
			request.setAttribute("error", "User limit exceeded Sixflags");
			e.printStackTrace();
		}
		request.setAttribute("listFolios", listFolios);
		request.getRequestDispatcher("/views/sixflags.jsp").forward(request, response);
	}

}
