package com.disney.firstWebSalvador.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.disney.projects.beans.Square;
import com.disney.projects.beans.Triangle;

/**
 * Servlet implementation class FigurasData
 */
@WebServlet("/figuras")
public class FigurasData extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor.
	 */
	public FigurasData() {
		// TODO Auto-generated constructor stub
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setAttribute("title", "Select a figure:");
		request.getRequestDispatcher("/views/Figuras.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		System.out.println(request.getParameter("radio"));
		if (request.getParameter("radio") != null) {

			int radio = Integer.parseInt(request.getParameter("radio"));
			if (radio == 1) {
				int lado = Integer.parseInt(request.getParameter("side"));
				Square square = new Square(lado);
				square.calculateArea();
				request.setAttribute("fullData", "The area of figure :" + square.getArea());

			} else {
				float base = Float.parseFloat(request.getParameter("base"));
				float tall = Float.parseFloat(request.getParameter("tall"));
				Triangle triangle = new Triangle(base, tall);
				triangle.calculateArea();
				request.setAttribute("fullData", "The area of figure :" + triangle.getArea());

			}
		}
		request.setAttribute("title", "Select a figure:");

		request.getRequestDispatcher("/views/Figuras.jsp").forward(request, response);

	}

}
