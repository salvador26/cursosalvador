package com.disney.firstWebSalvador.utils;

import java.util.ArrayList;

import com.disney.firstWebSalvador.exceptions.FolioRepeatException;
import com.disney.firstWebSalvador.exceptions.SixFlagsCapacityException;

public class ServletUtils {

	public static final int TOTAL = 10;

	public static void validateFolioRepeat(ArrayList<String> lista, String folio) {
		System.out.println("folio es:::::  " + folio);
		if (lista.contains(folio)) {
			throw new FolioRepeatException();
		}
	}

	public static void validateCapacityUsers(ArrayList<String> lista) throws SixFlagsCapacityException {
		if (lista.size() == TOTAL) {
			throw new SixFlagsCapacityException();

		}
	}

}
